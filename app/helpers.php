<?php
function chart(){
    $args = func_get_args();
   return call_user_func_array(['Chart', 'make'], $args);
}

function chart_top(){
    $args = func_get_args();
    return call_user_func_array(['Chart', 'makeTop'], $args);
}

function redirect_to($route){
  if ($_SERVER['REQUEST_URI'] != $route){
    header("Location: $route");
    exit;
  }
}
