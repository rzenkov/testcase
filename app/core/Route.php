<?php

class Route {
    private static $routes = [];
    public static function add($method, $pattern, $callback){
        $pattern = '/^' . str_replace('/', '\/',$pattern) . '$/';
        self::$routes[$method][$pattern] = $callback;
    }

    public static function get($pattern, $callback){
        self::add('GET', $pattern, $callback);
    }

    public static function post($pattern, $callback){
        self::add('POST', $pattern, $callback);
    }

    public static function dispatch($method, $request_uri){
        $url = explode('?', $request_uri)[0];
        foreach(self::$routes[$method] as $pattern => $callback){
            if(preg_match($pattern, $url, $params)){
                array_shift($params);
                if(is_array($callback)){
                    $callback[0] = new $callback[0];
                }
                return call_user_func_array($callback, array_values($params));
            }
        }
        throw new RouteNotFoundException("Route not found!");
    }
}