<?php

class App {
    public function run(){

        try {
            header('Content-Type: text/html; charset=utf-8');

            include_once __DIR__."/../routes.php";

            Route::dispatch($_SERVER['REQUEST_METHOD'], $_SERVER['REQUEST_URI']);

        } catch (RouteNotFoundException $e){
            return View::make('404', ['message'=>$e->getMessage()], 404);
        } catch (PDOException $e){
            redirect_to('import');
        } catch (Exception $e){
            return View::make('500', ['message'=>$e->getMessage()], 500);
        }

    }
}
