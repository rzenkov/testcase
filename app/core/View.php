<?php

class View {
    public static function make($view, $data=[], $code = 200){
        $file = __DIR__. "/../views/$view".".html";
        if(file_exists($file)){
            http_response_code($code);
            extract($data);
            return include $file;
        }
        throw new Exception("Couldn't find view $view");
    }
}