<?php

class Input {
    private static $input;
    public static function exists($key){
        return array_key_exists($key, self::$input);
    }
    public static function get($key){
        return self::$input[$key];
    }
    public static function getInput(){
        switch($_SERVER['REQUEST_METHOD']){
            case "GET": self::$input = $_GET;
                break;
            case "POST": self::$input = $_POST;
                break;
            default: self::$input = $_REQUEST;
        }

    }
} Input::getInput();
