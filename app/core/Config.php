<?php

class Config {
    private static $config = [];

    public static function get($key){
        if(array_key_exists($key, self::$config)){
            return self::$config[$key];
        }
        throw new Exception("Config key $key does not exists!");
    }

    public static function set($key, $value){
        self::$config[$key] = $value;
    }
}