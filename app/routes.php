<?php
Route::get('/report/*(\w+)*/*', ['GiftsController', 'index']);

Route::get('/import/*(\w+)*/*(\w+)*', function($id=null, $p=null){
    $repo = new GiftsRepository;
    $repo->createTables();

    return View::make('import');
});
Route::post('/import/*', function(){
    $repo = new GiftsRepository;
    if(array_key_exists('file', $_FILES)){
        $file = $_FILES['file'];
        if(!$file['error']){
            // Сохраняем загруженный файл в upload
            $uploaded = __DIR__ . '/../upload/'.$file['name'];
            move_uploaded_file($file['tmp_name'], $uploaded);
            LogParser::parseToDb($uploaded);
            unlink($uploaded);
            redirect_to('/');
        } else {
          throw new Exception("Can't upload file");
        }
    }

});
Route::get('/', ['MainController', 'index']);
