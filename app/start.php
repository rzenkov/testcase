<?php

spl_autoload_register('loadClass');

function loadClass($class){
    if( $file = findFile($class)){
        includeFile($file);
        return true;
    }
}

function includeFile($file){
    include $file;
}

function findFile($class){
    $applicationFolders = ['services', 'core', 'core'.DIRECTORY_SEPARATOR.'exceptions', 'controllers'];
    foreach($applicationFolders as $folder){
        $file = __DIR__ . DIRECTORY_SEPARATOR . $folder . DIRECTORY_SEPARATOR . $class . ".php";
        if(file_exists($file)){
            return $file;
        }
    }
    throw new Exception("Couldn't find class: $class");
}

Config::set('dbname', getenv('MYSQL_DBNAME')? getenv('MYSQL_DBNAME'):'testcase');
Config::set('dbuser', getenv('MYSQL_DBUSER')? getenv('MYSQL_DBUSER'): 'root');
Config::set('dbpass', getenv('MYSQL_DBPASS')? getenv('MYSQL_DBPASS'): '');
Config::set('dbhost', getenv('MYSQL_DBHOST')? getenv('MYSQL_DBHOST'): 'localhost');

$app = new App();

include_once 'helpers.php';

return $app;
