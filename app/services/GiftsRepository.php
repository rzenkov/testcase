<?php

class GiftsRepository extends CommonRepository{

    public function findAll(){
        $stmt = $this->db->prepare('select * from gifts');
        $stmt->execute();
        return $stmt->fetchAll();
    }
    public function findWhereFrom($from){
        $stmt = $this->db->prepare('select * from gifts where from_user_id = :from');
        $stmt->bindParam(':from', $from, PDO::PARAM_INT);
        $stmt->execute();
        return $stmt->fetchAll();
    }
    public function findWhereTo($to){
        $stmt = $this->db->prepare('select * from gifts where to_user_id = :to');
        $stmt->bindParam(':to', $to, PDO::PARAM_INT);
        $stmt->execute();
        return $stmt->fetchAll();
    }
    public function findWhereGift($gift){
        $stmt = $this->db->prepare('select * from gifts where gift_code = :gift');
        $stmt->bindParam(':gift', $gift, PDO::PARAM_INT);
        $stmt->execute();
        return $stmt->fetchAll();
    }

    public function getGiftSummary(){
        $stmt = $this->db->prepare('select gift_code, count(*) as gift_count from gifts group by gift_code order by gift_count desc');
        $stmt->execute();
        return $stmt->fetchAll();
    }
}