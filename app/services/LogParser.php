<?php

class LogParser {
    //parse loaded file, save parsed data into tempfile, return tempfile name;
    public static function parse($file){
        $pattern = "/'from_user_id'='(\d+)','to_user_id'='(\d+)','gift_code'='(\d+)','stime'='(\d+)','ttime'='*(\d+|\w+)'*/";
        $tmp = __DIR__.'/../../upload/uploaded.log';
        $in = fopen($file, 'r');
        $out = fopen($tmp, 'w');
        chmod($tmp, 0644);
        while(!feof($in)){
            $line = fgets($in);
            $res = preg_replace($pattern, "$1,$2,$3,$4,$5", $line);
            fwrite($out, $res);
        }
        fclose($in);
        fclose($out);
        return $tmp;
    }

    public static function parseToDb($file){
        $pattern = "/'from_user_id'='(\d+)','to_user_id'='(\d+)','gift_code'='(\d+)','stime'='(\d+)','ttime'='*(\d+|\w+)'*/";
        $input = fopen($file, 'r');
        $count = 0;
        $repo = new CommonRepository();
        $repo->truncate();
        while(!feof($input)){
          $line = fgets($input);
          preg_match($pattern, $line, $parsed);
          array_shift($parsed);

          if($count == 0){
            $repo->beginTransaction();
          }

          $repo->insertArray($parsed);
          $count++;
          if($count == 40){
            $repo->commit();
            $count = 0;
          }

        }
        fclose($input);
    }
}
