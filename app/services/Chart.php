<?php

class Chart{
    public static function make($data, $key, $type='bvg', $size="600x300"){
        if(count($data) == 0){
            return "";
        }
        $values = [];
        $chart = "https://chart.googleapis.com/chart?chs=$size&cht=$type&chbh=6,3,2&chds=0,3300&chdlp=t";
        foreach($data as $row){
            $values[] = $row[$key];
        }
        $chart .= '&chd=t:'.implode(',', $values);

        return "<img src=\"$chart\">";
    }

    public static function makeTop($data, $valueKey, $nameKey, $type='p3', $size="600x300"){
        if(count($data) == 0){
            return "";
        }
        $values = [];
        $names = [];
        $chart = "https://chart.googleapis.com/chart?chs=$size&cht=$type&chbh=6,3,2&chds=0,3300&chdlp=t";
        $chart .= "&chco=ff0000,00ff00,0000ff,ffff00,00ffff";
        $limit = 20;
        if(count($data)<20){
            $limit = count($data);
        }
        for($i = 0; $i < $limit; $i++){
            $values[] = $data[$i][$valueKey];
            $names[] = $data[$i][$nameKey].' ('.$data[$i][$valueKey].')';
        }
        $chart .= '&chd=t:'.implode(',', $values);
        $chart .= '&chl='.implode('|', $names);

        return "<img src=\"$chart\">";

    }

}
