<?php

class CommonRepository{
    protected $db;

    public function __construct(){
        $host = Config::get('dbhost');
        $user = Config::get('dbuser');
        $pass = Config::get('dbpass');
        $base = Config::get('dbname');
        $this->db = new PDO("mysql:host=$host;dbname=$base", $user, $pass);
        $this->db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    }

    public function createTables(){
        $ddl = "create table if not exists "
        ."gifts (from_user_id int, to_user_id int, gift_code int, stime timestamp, ttime timestamp) "
        ."default charset=utf8";
        $this->db->query($ddl);
    }

    public function importCSV($file){
        $this->db->query('truncate gifts');
        $query = "load data infile '$file' into table gifts fields terminated by ',' (from_user_id, to_user_id, gift_code, @st, @tt) set stime=FROM_UNIXTIME(@st), ttime = FROM_UNIXTIME(@tt)";
        $this->db->query($query);
    }

    public function insertArray($values){
          $query = "insert into gifts values(:from, :to, :code, FROM_UNIXTIME(:stime), FROM_UNIXTIME(:ttime))";
          $stmt = $this->db->prepare($query);
          $stmt->bindParam(':from', $values[0]);
          $stmt->bindParam(':to', $values[1]);
          $stmt->bindParam(':code', $values[2]);
          $stmt->bindParam(':stime', $values[3]);
          $stmt->bindParam(':ttime', $values[4]);
          $stmt->execute();

    }

    public function truncate(){
      $this->db->query('truncate gifts');
    }


    public function beginTransaction(){
      $this->db->beginTransaction();
    }
    public function commit(){
      $this->db->commit();
    }
}
