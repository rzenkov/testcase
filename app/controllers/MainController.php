<?php

class MainController{
    public function index(){
    $repo = new GiftsRepository;
    $gifts = $repo->getGiftSummary();

    return View::make('summary', ['gifts' => $gifts]);
    }
}