<?php


class GiftsController {
    public function index($id = null){
        $repo = new GiftsRepository;
        if(Input::exists('from')){
            $from = Input::get('from');
            $gifts = $repo->findWhereFrom($from);
        } elseif(Input::exists('to')){
            $to = Input::get('to');
            $gifts = $repo->findWhereTo($to);
        } elseif(Input::exists('code')){
            $gift = Input::get('code');
            $gifts = $repo->findWhereGift($gift);
        }else {
            $gifts = $repo->findAll();
        }

        return View::make('report', ['gifts' => $gifts]);
    }
}