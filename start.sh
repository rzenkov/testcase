#!/bin/sh

echo "set env vars before run script"
echo "default values:"
echo "MYSQL_DBHOST = localhost"
echo "MYSQL_DBNAME = testcase"
echo "MYSQL_DBUSER = root"
echo "MYSQL_DBPASS = ''"
echo ""
echo "listening http://localhost:8080"

php -S >localhost localhost:8080 -t . public/index.php
